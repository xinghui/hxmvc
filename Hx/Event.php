<?php

namespace Hx;

/**
 * 事件调用类
 * Class Event
 * @package Hx
 */
class Event {

    /**
     * 注册的事件
     * @var array
     */
    private static $events = array();

    /**
     * 被调用的事件名称
     * @var array
     */
    private static $used = array();

    /**
     * 直接注册，而不是添加
     * @param array $array
     */
    public static function reg(array $array) {
        self::$events += $array;
    }

    /**
     * 添加事件
     * @param string $name
     * @param array|\Closure $func
     */
    public static function on($name, $func) {
        self::$events[$name][] = $func;
    }

    /**
     * 移除事件
     * 移除所有
     * @param string $name
     */
    public static function off($name) {
        unset(self::$events[$name]);
    }

    /**
     * 触发事件
     * @param string $name
     * @param array $arg
     * @return mixed|null
     */
    public static function trigger($name, $arg = array()) {
        #记住调用的事件
        self::$used[] = $name;
        $evt = self::$events[$name];
        if ($evt) {
            foreach ($evt as $e) {
                if (is_array($e)) {
                    $event = App::getApp('name') . '\\App\Event\\' . $e[0];
                    if (count($evt) < 2) {
                        return call_user_func_array(array($event, $e[1]), $arg);
                    }
                    call_user_func_array(array($event, $e[1]), $arg);
                }
                if ($e instanceof \Closure) {
                    return call_user_func_array($e, $arg);
                }
            }
        }
        return null;
    }

    /**
     * 获取所有被调用的事件
     * @return array
     */
    public static function all() {
        return array_unique(self::$used);
    }
}